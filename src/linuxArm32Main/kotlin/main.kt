package org.example.eflHello

import io.gitlab.embedSoft.eflKt.core.event.EflEvent
import io.gitlab.embedSoft.eflKt.ui.startDesktopApplication

fun main(args: Array<String>) {
    startDesktopApplication(args, ::eflMain)
}

@Suppress("UNUSED_PARAMETER")
private fun eflMain(event: EflEvent) {
    setupMainWindow()
}

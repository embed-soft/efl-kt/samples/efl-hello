group = "org.example"
version = "0.1"

plugins {
    kotlin("multiplatform") version "1.5.30"
}

repositories {
    mavenCentral()
    mavenLocal()
}

kotlin {
    val eflKtVer = "0.1"
    val embedSoftGroupId = "io.gitlab.embed-soft"
    val mainFunction = "org.example.eflHello.main"
    val binaryName = "efl_hello"
    linuxX64 {
        compilations.getByName("main") {
            cinterops.create("efl") {
                val baseIncludeDir = "/usr/include"
                includeDirs(
                    "$baseIncludeDir/ecore-1",
                    "$baseIncludeDir/ecore-con-1",
                    "$baseIncludeDir/efl-1",
                    "$baseIncludeDir/eina-1",
                    "$baseIncludeDir/eina-1/eina",
                    "$baseIncludeDir/eo-1",
                    "$baseIncludeDir/elementary-1",
                    "$baseIncludeDir/evas-1",
                    "$baseIncludeDir/edje-1"
                )
            }
        }
        binaries {
            executable(binaryName) {
                entryPoint = mainFunction
            }
        }
    }
    linuxArm32Hfp("linuxArm32") {
        compilations.getByName("main") {
            dependencies {
                implementation("$embedSoftGroupId:eflkt-core:$eflKtVer")
                implementation("$embedSoftGroupId:eflkt-ui:$eflKtVer")
            }
            cinterops.create("efl") {
                val baseIncludeDir = "/mnt/pi_image/usr/local/include"
                includeDirs(
                    "$baseIncludeDir/ecore-1",
                    "$baseIncludeDir/ecore-con-1",
                    "$baseIncludeDir/efl-1",
                    "$baseIncludeDir/eina-1",
                    "$baseIncludeDir/eina-1/eina",
                    "$baseIncludeDir/eo-1",
                    "$baseIncludeDir/elementary-1",
                    "$baseIncludeDir/evas-1",
                    "$baseIncludeDir/edje-1",
                    "$baseIncludeDir/emile-1",
                    "$baseIncludeDir/eio-1",
                    "$baseIncludeDir/eet-1",
                    "$baseIncludeDir/eldbus-1",
                    "$baseIncludeDir/efreet-1",
                    "$baseIncludeDir/ethumb-client-1",
                    "$baseIncludeDir/ethumb-1"
                )
            }
        }
        binaries {
            executable(binaryName) {
                entryPoint = mainFunction
            }
        }
    }
}
